import path    from 'path'

import webpack from 'webpack'
import dotenv  from 'dotenv'

import ExtractTextPlugin from 'extract-text-webpack-plugin'

dotenv.config()

const PATHS      = { }

PATHS['base']    = __dirname
PATHS['project'] = path.join(PATHS['base'], process.env.PROJECT || 'echo')

PATHS['static']  = path.join(PATHS['project'], 'static')
PATHS['assets']  = path.join(PATHS['project'], 'assets')

const extract    = new ExtractTextPlugin({
    filename: path.join(PATHS['assets'], "styles.css"),
     disable: process.env.ECHO_ENVIRONMENT == 'development'
})

export default {
    entry: path.join(PATHS['static'], 'app', 'index.js'),
    output: {
        path: path.join(PATHS['assets'], 'js'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: "babel-loader"
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: extract.extract({
                    use: ["css-loader", "sass-loader"],
                    fallback: "style-loader"
                })
            }
        ]
    },
    plugins: [
        extract
    ],
    resolve: {
        modules: [
            path.join(PATHS['base'], 'node_modules'),
            path.join(PATHS['static'], 'scss')
        ]
    }
}