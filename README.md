<div align="center">
	<h1>DjangoSheet</h1>
</div>

#### 0. Installation

Prerequisites - [Python](https://www.python.org/downloads), [PIP](https://pip.pypa.io/en/stable/installing).

```
$ pip install django
```

##### 1. Create a New Django Project

```
$ django-admin startproject PROJECT
```

###### Directory

```
echo
├── echo
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py
```

##### 2. Start the Development Server

```console
$ python manage.py runserver HOST:PORT
```

##### 3. Create a Django App

```console
$ python manage.py startapp APP
```

##### 4. Install a Django App to a Project

```python
# project/project/settings.py
INSTALLED_APPS = [
	# django-apps,
	# ...,
	# ...,
	# ...,
	'myapp'
]
```

##### 5. Create a View

```python
# project/app/views.py
from django.http import HttpResponse

def index(request):
	return HttpResponse("Hello, World!")
```

##### 5. Map Views to App URLs

```python
# project/app/urls.py
from django.urls import path
from myapp import views

urlpatterns = [
	path('', views.index)
]
```

##### 6. Register App URLs to Project URLs

```python
# project/project/urls.py
from django.urls import path, include

urlpatterns = [
	path('myapp', include('myapp.urls')
]
```

##### 7. Create App Templates

```python
# project/project/settings.py
import os

TEMPLATES = [
	# ...
	DIRS: [
		os.path.join(BASE_DIR, 'app', 'templates')
	]
	# ...
]
```

```html
<!-- project/app/templates/app/index.html -->
<!-- Your HTML Layout -->
```

```python
# project/app/views.py
from django.shortcuts import render

def index(request):
	template = render(request, 'base/index.html')
	return template
```