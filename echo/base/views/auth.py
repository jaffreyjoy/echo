from django.shortcuts    import render
from django.contrib.auth import authenticate, login

from base.forms          import UserForm

def signin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user     = authenticate(
            username = username,
            password = password
        )

        if user:
            if user.is_active:
                login(username = username, password = password)
                # TODO: Redirect
            else:
                # TODO: Error
                pass
        else:
            # TODO: Error
            pass
    else:
        template = render(request, 'base/pages/auth/signin.html')
        return template

def signup(request):
    signed_up = False
    context   = dict(
        signed_up = signed_up,
        form      = UserForm()
    )
    template  = render(request, 'base/pages/auth/signup.html', context)
    return template