from django.db import models

class Category(models.Model):
    name = models.CharField(
        max_length = 128,
        unique     = True
    )

    def __unicode__(self):
        return '<Category {name}>'.format(
            name = self.name
        )